require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given(:taxonomy)  { create(:taxonomy) }
  given(:taxon1)    { create(:taxon, taxonomy: taxonomy, name: "Clothing",    parent_id: taxonomy.root.id) }
  given(:taxon2)    { create(:taxon, taxonomy: taxonomy, name: "T-Shirts",    parent_id: taxon1.id) }
  given(:taxon3)    { create(:taxon, taxonomy: taxonomy, name: "Bags",        parent_id: taxonomy.root.id) }
  given(:taxon4)    { create(:taxon, taxonomy: taxonomy, name: "No products", parent_id: taxonomy.root.id) }
  given!(:product1) { create(:product, name: "Ruby T-Shirts", taxons: [taxon2], price: 20) }
  given!(:product2) { create(:product, name: "Rails Bag",     taxons: [taxon3], price: 30) }
  background do
    visit potepan_category_path taxon2.id
  end

  scenario "display correct categories page" do
    expect(page).to have_title taxon2.name
    within ".productBox" do
      expect(page).to have_content     product1.name
      expect(page).to have_content     product1.display_price
      expect(page).not_to have_content product2.name
      expect(page).not_to have_content product2.display_price
    end
    within ".sideBar" do
      expect(page).to have_content     taxonomy.name
      expect(page).to have_content     taxon2.name
      expect(page).to have_content     taxon3.name
      expect(page).not_to have_content taxon1.name
      expect(page).not_to have_content taxon4.name
    end
  end

  scenario "move correct page from category link" do
    click_on taxon3.name
    expect(current_path).to eq potepan_category_path taxon3.id
    expect(page).to have_title       taxon3.name
    expect(page).to have_content     product2.name
    expect(page).to have_content     product2.display_price
    expect(page).not_to have_title   taxon2.name
    expect(page).not_to have_content product1.name
    expect(page).not_to have_content product1.display_price
  end

  scenario "move correct page from product link" do
    click_on product1.name
    expect(current_path).to eq potepan_product_path product1.id
    expect(page).to have_title       product1.name
    expect(page).to have_content     product1.name
    expect(page).to have_content     product1.display_price
    expect(page).not_to have_title   product2.name
    expect(page).not_to have_content product2.name
    expect(page).not_to have_content product2.display_price
    # 前ページがカテゴリー一覧ページの場合、前のページへのリンクがあること
    expect(page).to have_link "一覧ページへ戻る"
    expect(page).not_to have_link "商品一覧へ"
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path taxon2.id
    expect(current_path).not_to eq potepan_products_path
  end
end
