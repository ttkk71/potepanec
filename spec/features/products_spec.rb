require 'rails_helper'

RSpec.feature "Products", type: :feature do
  feature "display products page detail" do
    given(:taxonomy)           { create(:taxonomy) }
    given(:taxon1)             { create(:taxon, taxonomy: taxonomy, name: "Clothing", parent_id: taxonomy.root.id) }
    given(:taxon2)             { create(:taxon, taxonomy: taxonomy, name: "Bags",     parent_id: taxonomy.root.id) }
    given(:taxon3)             { create(:taxon, taxonomy: taxonomy, name: "T-Shirts", parent_id: taxon1.id) }
    given(:taxon4)             { create(:taxon, taxonomy: taxonomy, name: "Rails",    parent_id: taxonomy.root.id) }
    given(:product1)           { create(:product, taxons: [taxon1, taxon3, taxon4], price: 20, description: "foo") }
    given(:product2)           { create(:product, taxons: [taxon2], price: 15, description: "bar") }
    given(:property1)          { create(:property, products: [product1]) }
    given(:property2)          { create(:property, products: [product2]) }
    given!(:product_property1) { create(:product_property, product: product1, property: property1) }
    given!(:product_property2) { create(:product_property, product: product2, property: property2) }
    given!(:related_product)   { create(:product, taxons: [taxon1, taxon3, taxon4], price: 25) }
    given!(:unrelated_product) { create(:product, taxons: [taxon2], price: 30) }

    feature "display products show page" do
      background do
        visit potepan_product_path product1.id
      end

      scenario "display correct product and related products" do
        within ".singleProduct" do
          expect(page).to have_title   product1.name
          expect(page).to have_content product1.name
          expect(page).to have_content product1.display_price
          expect(page).to have_content product1.description
          expect(page).to have_content property1.name
          expect(page).to have_content product_property1.value
          expect(page).not_to have_title   product2.name
          expect(page).not_to have_content product2.name
          expect(page).not_to have_content product2.display_price
          expect(page).not_to have_content product2.description
          expect(page).not_to match_array  property2.name
          expect(page).not_to match_array  product_property2.value
        end
        within ".productsContent" do
          # 関連商品が表示され、かつ重複がないこと
          expect(page).to have_content related_product.name, count: 1
          expect(page).to have_content related_product.display_price, count: 1
          expect(page).not_to have_content unrelated_product.name
          expect(page).not_to have_content unrelated_product.display_price
          # メイン商品が関連商品に表示されないこと
          expect(page).not_to have_content product1.name
          expect(page).not_to have_content product1.display_price
        end
      end

      scenario "move correct page from related product link" do
        click_on related_product.name
        expect(current_path).to eq potepan_product_path related_product.id
        expect(page).to have_title   related_product.name
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
        expect(page).not_to have_title   unrelated_product.name
        expect(page).not_to have_content unrelated_product.name
        expect(page).not_to have_content unrelated_product.display_price
      end
    end

    feature "display products index page" do
      background do
        visit potepan_products_path
      end

      scenario "display all products and categories" do
        expect(page).to have_title "ALL PRODUCTS"
        expect(page).to have_content product1.name
        expect(page).to have_content product1.display_price
        expect(page).to have_content product2.name
        expect(page).to have_content product2.display_price
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
        expect(page).to have_content unrelated_product.name
        expect(page).to have_content unrelated_product.display_price
        expect(page).to have_content     taxonomy.name
        expect(page).to have_content     taxon2.name
        expect(page).to have_content     taxon3.name
        expect(page).to have_content     taxon4.name
        expect(page).not_to have_content taxon1.name
      end

      scenario "move correct page from product link" do
        click_on product1.name
        expect(current_path).to eq potepan_product_path product1.id
        expect(page).to have_title       product1.name
        expect(page).to have_content     product1.name
        expect(page).to have_content     product1.display_price
        expect(page).not_to have_title   product2.name
        expect(page).not_to have_content product2.name
        expect(page).not_to have_content product2.display_price
        # 商品一覧ページへのリンクがあること
        expect(page).to have_link "商品一覧へ"
        expect(page).not_to have_link "一覧ページへ戻る"
        click_on "商品一覧へ"
        expect(current_path).to eq potepan_products_path
        expect(current_path).not_to eq potepan_category_path taxon1.id
        expect(current_path).not_to eq potepan_category_path taxon3.id
        expect(current_path).not_to eq potepan_category_path taxon4.id
      end
    end
  end
end
