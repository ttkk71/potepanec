require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#get_related_products" do
    let(:taxon1)            { create(:taxon) }
    let(:taxon2)            { create(:taxon) }
    let(:product)           { create(:product, taxons: [taxon1]) }
    let(:related_product)   { create(:product, taxons: [taxon1]) }
    let(:unrelated_product) { create(:product, taxons: [taxon2]) }
    let(:related_products)  { Spree::Product.get_related_products(product) }

    it "doesn't get source product" do
      expect(related_products).not_to include(product)
    end

    it "doesn't get unrelated products" do
      expect(related_products).not_to include(unrelated_product)
    end
  end
end
