require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxonomy)   { create(:taxonomy) }
    let(:taxon)      { create(:taxon, taxonomy: taxonomy) }
    let!(:product1)  { create(:product, taxons: [taxon]) }
    let!(:product2)  { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "returns status 200" do
      expect(response.status).to eq(200)
    end

    it "renders show page" do
      expect(response).to render_template :show
    end

    it "assigns correct taxon" do
      expect(assigns(:taxon)).to eq(taxon)
    end

    it "assigns correct taxonomy" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end

    it "assigns correct product" do
      expect(assigns(:products)).to match_array(product1)
    end

    it "doesn't assign other taxons product" do
      expect(assigns(:products)).not_to include(product2)
    end
  end
end
