require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET actions" do
    let(:taxonomy)              { create(:taxonomy) }
    let(:taxon1)                { create(:taxon, taxonomy: taxonomy) }
    let(:taxon2)                { create(:taxon, taxonomy: taxonomy) }
    let(:taxon3)                { create(:taxon, taxonomy: taxonomy) }
    let(:taxon4)                { create(:taxon, taxonomy: taxonomy) }
    let(:product)               { create(:product, taxons: [taxon1, taxon3, taxon4]) }
    let!(:related_product1)     { create(:product, taxons: [taxon1, taxon3, taxon4]) }
    let!(:related_product2)     { create(:product, taxons: [taxon1, taxon3]) }
    let!(:related_product3)     { create(:product, taxons: [taxon1, taxon4]) }
    let!(:related_product4)     { create(:product, taxons: [taxon1]) }
    let!(:unrelated_product)    { create(:product, taxons: [taxon2]) }
    let(:all_related_products)  { [related_product1, related_product2, related_product3, related_product4] }

    describe "GET #show" do
      before do
        get :show, params: { id: product.id }
      end

      it "return status 200" do
        expect(response.status).to eq(200)
      end

      it "renders show page" do
        expect(response).to render_template :show
      end

      it "assigns correct product" do
        expect(assigns(:product)).to eq(product)
      end

      it "assigns related products uniquely" do
        expect(assigns(:related_products)).to match_array(all_related_products)
      end

      context "when there are 5 related products" do
        let!(:related_product5) { create(:product, taxons: [taxon1]) }

        it "assigns 4 related products" do
          expect(assigns(:related_products).size).to eq 4
        end
      end
    end

    describe "GET #index" do
      before do
        get :index
      end

      it "return status 200" do
        expect(response.status).to eq(200)
      end

      it "renders index page" do
        expect(response).to render_template :index
      end

      it "assigns all products" do
        expect(assigns(:products)).to match_array(Spree::Product.all)
      end

      it "assigns correct taxonomy" do
        expect(assigns(:taxonomies)).to match_array(taxonomy)
      end
    end
  end
end
