require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    base_title = "BIGBAG Store"

    # デフォルト
    it "when there is no argument, return default title" do
      expect(helper.full_title).to eq(base_title)
    end

    # title指定あり
    it "when there is argument, return the title corresponding to argument" do
      expect(helper.full_title("hoge")).to eq("hoge - #{base_title}")
    end
  end
end
