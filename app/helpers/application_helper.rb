module ApplicationHelper
  def full_title(page_title = '')
    base_title = "BIGBAG Store"
    if page_title.empty?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def partners_image_tag(logo)
    image_tag("img/home/partners/#{logo}", :alt => "partner-img")
  end
end
