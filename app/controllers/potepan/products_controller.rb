class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_OF_RELATED_PRODUCTS = 4

  def index
    @products = Spree::Product.all.includes(master: [:images, :default_price])
    @taxonomies = Spree::Taxonomy.includes(:root)
  end

  def show
    @product = Spree::Product.find(params[:id])
    @product_properties = @product.product_properties.includes(:property)
    @related_products = Spree::Product.get_related_products(@product).
                        order(Arel.sql("RAND()")).limit(MAX_NUMBER_OF_RELATED_PRODUCTS)
  end
end
