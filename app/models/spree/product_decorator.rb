module Spree::ProductDecorator
  def get_related_products(product)
    in_taxons(product.taxons).
    includes(master: [:images, :default_price]).
    where.not(id: product.id).
    distinct
  end
  Spree::Product.extend self
end
